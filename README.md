# panopticon

Bot discord pour GN virtuels.


## Prérequis

Al vous faudra juste le module discord.py : <https://discordpy.readthedocs.io/>) 

## Démarrage
* Créez votre application et votre bot Discord sur la console de développement Discord (<https://discord.com/developers/>), et invitez le sur le serveur avec des droits administrateur. (Si vous ne savez pas faire cela, y'a des tutos en ligne, je tacherai de mettre un lien vers un bien.)
* Créez le fichier `config.json` a partir du `config-template-v2.json` et remplissez les champs suivants de la section `server` :
  * `token` : le token de votre bot (obligatoire)
  * `server_managed` : l'ID de votre serveur (obligatoire)
  * `gamemaster_ids` et/ou `gamemaster_role_id` : définit respectivement une liste d'IDs d'utilisataire et/ou un ID de rôle, qui seront reconnus comme MJ par le bot. Al est obligatoire de définir au moins l'un des deux.
  * `log_channel_id` : l'ID d'un canal qui sera utilisé par le bot comme console de logs; cela doit être un canal visible uniquement des MJs et du bot (obligatoire).
* Vous pouvez aussi remplir les champs de la section `bot` pour changer la devise, la phrase de présentation et le nom utilisé par le bot.
* Lancez le script : `python3 panopticon.py`
