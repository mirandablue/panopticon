#!/usr/bin/python
# -*- coding: utf-8 -*-

""" Alexandre Coninx
    ISIR CNRS/UPMC
    07/07/2019
""" 


import discord

from datetime import datetime

from asyncio import Lock

import json

import sys, os
from discord.ext import commands
from discord.ext.commands import CommandNotFound, CheckFailure

from server import Faction

version = "0.31"
config_file = "config.json"

# Defaults - can be overriden by config["bot"]
botname = "Panopticon"
hello_phrase = "A panopticon is a system where the center can watch the other people, and the other people can't know if they are being watched."
currency = "$"



try:
    with open(config_file,"r") as fd:
        config = json.load(fd)
    if "version" not in config: # Old version 0 file
        token = config['token']
        server_managed = config['server_managed']
        gamemaster_ids = config['gamemaster_ids'] if 'gamemaster_ids' in config else []
        gamemaster_role_id = config['gamemaster_role_id'] if 'gamemaster_role_id' in config else -1
        log_channel_id = config['log_channel_id']
    elif config["version"] == 1: # Version 1
        serverconfig = config["server"][0]
        token = serverconfig['token']
        server_managed = serverconfig['server_managed']
        gamemaster_ids = serverconfig['gamemaster_ids'] if 'gamemaster_ids' in serverconfig else []
        gamemaster_role_id = serverconfig['gamemaster_role_id'] if 'gamemaster_role_id' in serverconfig else -1
        log_channel_id = serverconfig['log_channel_id']
        if "bot" in config: # Bot config - hello phrase, currency, etc.
            botconfig = config["bot"][0]
            if "currency" in botconfig:
                currency = botconfig["currency"]
            if "botname" in botconfig:
                botname = botconfig["botname"]
            if "hello_phrase" in botconfig:
                hello_phrase = botconfig["hello_phrase"]
    else:
        raise RuntimeError("Unknown config.json version %s" % str(config["version"]))
        
        
except FileNotFoundError:
    print("Error: can't find file %s " % config_file)
    sys.exit(1)
except json.JSONDecodeError:
    print("Error: %s is not a well formed JSON file" % config_file)
    sys.exit(1)
except KeyError as e:
    print("Error: Missing critical config parameter '%s'" % e.args[0])
    sys.exit(1)

if(not gamemaster_ids and (gamemaster_role_id == -1)):
    raise RuntimeError("Error: you must specify at least a gamemaster_role_id or one or more gamemaster_ids")


server_save_file = "%d.json" % server_managed


def get_name(author):
    if isinstance(author, discord.Member):
        return (author.nick if author.nick is not None else author.name)
    elif isinstance(author, discord.User): # it's a User
        return author.name
    else:
        return "<unknown user, this is an error>"


class BotClient(commands.Bot):
    
    def __init__(self, *args, **kwargs):
        super(BotClient, self).__init__(*args, **kwargs)
        # NOT init
        self.is_init = False
        # Attributes
        self.state_lock=Lock()
        self.factions = {}
        self.bank_accounts = {}
        self.codenames = {}
        
    
    async def on_ready(self):
        self.my_server = None
        for g in self.guilds:
            if(g.id == server_managed):
                self.my_server = g
                print("Serveur %s trouvé" % g.name)
        if(self.my_server is None):
            print("Erreur : invitez moi sur le serveur #%d !" % server_managed)
            sys.exit(1)
        self.log_channel = self.my_server.get_channel(log_channel_id)
        if self.log_channel is None:
            print("Erreur : impossible de trouver le canal de log d'ID #%d !" % log_channel_id)
            sys.exit(1)
            
        if(not self.is_init):
            print("{} : je suis connectæ sous le nom {} et l'ID {}".format(datetime.now().strftime("%d/%m %H:%M:%S"), self.user.name, self.user.id))
            # Find server
            await self.log_raw("\n\n**__:eye::desktop: Panopticon v%s bonjour - connectæ au serveur '%s'.__**" % (version, self.my_server.name))

            if os.path.isfile(server_save_file):
                await self.log_raw("Fichier de sauvegarde trouvé, chargement en cours...")
                await load(ctx=self.log_channel)
            else:
                await self.log_raw("Aucun fichier de sauvegarde trouvé. A vous de jouer pour créer les factions.")
            await self.log_raw("**__:white_check_mark: Démarrage terminé, tout est opérationnel.__** **Envoyez moi $help en message privé** pour un résumé des commandes.")
            self.is_init = True
        else:
            print("{} : J'ai été déconnectæ et reconnectæ".format(datetime.now().strftime("%d/%m %H:%M:%S")))
            await self.log_raw("\n\n**:eye::desktop: J'ai été déconnectæ et reconnectæ**")
    
    async def log_raw(self, msg):
        await self.log_channel.send(msg)
    
    async def log(self, who, msg):
        whoname = get_name(self.uid_as_member(who.id))
        await self.log_channel.send("(**%s**) : %s" % (whoname + (" :mage:" if has_gm_privilege(who.id) else ""), msg))
    
    
    
    def uid_as_member(self, uid):
        return(self.my_server.get_member(uid))
        
    def find_by_name(self, nom, case_insensitive=True):
        """
        Find someone in the server.
        Will (by order of priority) try to find nom as an UID, a name or a nick
        Returns the (UID, display name) of the person if they were found, None otherwise
        """
        try:
            numeric_id = int(nom)
        except ValueError:
            numeric_id = None
        member = None
        if(numeric_id):
            member = self.my_server.get_member(numeric_id)
        if member:
            return (member.id, get_name(member))
        else: # Not found as UID
            for m in self.my_server.members:
                if((m.name == nom) or (case_insensitive and (m.name.lower() == nom.lower()))):
                    return (m.id, get_name(m))
            for m in self.my_server.members:
                if((m.nick is not None) and ((m.nick == nom) or (case_insensitive and (m.nick.lower() == nom.lower())))):
                    return (m.id, get_name(m))
        return None # not found
    
    def find_codename(self, nom, case_insensitive=True):
        if nom in self.codenames.keys():
            return (self.codenames[nom], get_name(self.uid_as_member(self.codenames[nom])))
        elif case_insensitive:
            for (code, uid) in self.codenames.items():
                if(code.lower() == nom.lower()):
                    return (uid, get_name(self.uid_as_member(uid)))
        return None #not found
    
    async def add_user_to_section(self, uid, cid):
        """ Add user uid to category cid.
            Just checks if the user and the category do exist, but not that they're not already member
        """
        category = self.my_server.get_channel(cid)
        if not category:
            return False # Section does not exist
        user = self.uid_as_member(uid)
        if not user:
            return False # User does not exist on the server
        await category.set_permissions(user, read_messages=True)
        return True

    async def rm_user_from_section(self, uid, cid):
        """ Add user uid to category cid
            Just checks if the user and the category do exist, but not that they're actually a member
        """
        category = self.my_server.get_channel(cid)
        if not category:
            return False # Section does not exist
        user = self.uid_as_member(uid)
        if not user:
            return False # User does not exist on the server
        await category.set_permissions(user, overwrite=None)
        return True
    
    def get_users_in_section(self, cid):
        """ Returns a list of users with an override to read the section
        """
        category = self.my_server.get_channel(cid)
        if not category:
            return None # Section does not exist
        overwrites = dict(category.overwrites) # All overwrites - users and roles
        overwrites_users_ok = list([k for k, v in overwrites.items() if isinstance(k, discord.Member) and (v.read_messages==True)])
        return overwrites_users_ok
        
    def get_money(self, uid):
        """ Returns the bank account of the given UID
            If the UID is unknown, checks that the person is in the server and add it to the accounts
        """
        num_uid=int(uid)
        if(num_uid not in self.bank_accounts):
            if(self.uid_as_member(num_uid) is not None):
                self.bank_accounts[num_uid] = 0
            else:
                return None # Error
        return self.bank_accounts[num_uid]

    def set_money(self, uid, money):
        """ Set the bank account of the given UID to the given amount
            If the UID is unknown, checks that the person is in the server and add it to the accounts
        """
        num_uid=int(uid)
        if(num_uid not in self.bank_accounts):
            if(self.uid_as_member(num_uid) is not None):
                self.bank_accounts[num_uid] = int(money)
            else:
                return None # Error
        self.bank_accounts[num_uid] = int(money)
    
    def get_codenames(self, uid):
        codes = list()
        for (k,v) in self.codenames.items():
            if(v == uid):
                codes.append(k)
        return codes


# Build intents - new in discordpy 1.5
intents = discord.Intents.default()
intents.guilds = True #for self.guilds
intents.members = True # privileged, but necessary
intents.messages = True # necessary for speaking on channels !
intents.dm_messages = True # # necessary for direct communication


client = BotClient(command_prefix='$', description="A panopticon is a system where the center can watch the other people, and the other people can't know if they are being watched.", activity=discord.Game(name="tapez $help"), help_command=None, intents=intents)


async def savedata(client):
    async with client.state_lock:
        factiondata = {k:f.todict() for (k,f) in client.factions.items()}
        accounts = dict(client.bank_accounts)
        codenames = dict(client.codenames)
        tosave = {"factiondata":factiondata, "accounts":accounts, "codenames":codenames, "version":2}
        with open(server_save_file,'w') as jsfile:
            jsfile.write(json.dumps(tosave, sort_keys=True, indent=4))


def has_gm_privilege(uid):
    if uid in gamemaster_ids:
        return True # explicit GM
    member = client.uid_as_member(uid)
    for r in member.roles:
        if r.id == gamemaster_role_id:
             return True # Has the GM role
    return False # Otherwise, nay


def is_gm():
    async def predicate(ctx):
        return has_gm_privilege(ctx.author.id)
    return commands.check(predicate)


def is_private():
    async def predicate(ctx):
        return ctx.guild is None
    return commands.check(predicate)




def has_group_member_rights(uid,faction_name):
     return ((uid in client.factions[faction_name].id_membres) or has_gm_privilege(uid))

def has_group_admin_rights(uid,faction_name):
    return ((uid in client.factions[faction_name].id_admins) or has_gm_privilege(uid))


commandes_membre = "  `$help`, ou `$hello`, ou `$statut` : **affiche votre compte en banque, les factions dont vous êtes membre et les commandes disponibles**\n  `$info_groupe <faction>` : donne la **liste des membres** et responsables de la faction\n"

commandes_groupadmin = "  `$ajouter_membre <personne> <faction>` : **ajoute une personne à une faction** dont vous êtes responsable\n  `$enlever_membre <personne> <faction>` : **exclue une personne d'une faction** dont vous êtes responsable\n"

commandes_gm = "  `$ajouter_groupe <nom> <section_discord> [<titre_admin>] [<titre_membre>]` : **crée une nouvelle faction**, et l'associe à une section du Discord. On peut customiser le titre des responsables et des membres (le défaut est \"Chef\" et \"Membre\")\n  `$liste_groupes` : **liste toutes les factions** existantes, tous les membres et responsables\n  `$ajouter_admin <personne> <faction>` : **ajoute ou promeut une personne comme responsable** d'une faction\n  `$enlever_admin <personne> <faction>` : **retire une personne comme responsable** d'une faction (elle redevient simple membre)\n  `$update` : met à jour les informations du bot avec l'état du serveur Discord (si vous avez ajouté ou enlevé des membres à la main)\n  `$modifier_groupe <faction> (salon|titre|titre_admin) <nouvelle valeur>` : modifie une faction en changeant le salon à laquelle elle est associée, le titre des membres ou celui des admins\n  `$ajouter_alias <personne> <nom de code>` : ajoute un nom de code à une personne\n  `$enlever_alias <nom de code> : supprime un nom de code\n  ``$aliases` : liste tous les noms de code existants\n"


commandes_gm_money = "  `$banque [+|-]` : **affiche les comptes en banque de tout le monde**, par ordre alphabétique (défaut), décroissant (-) ou croissant (+)\n  `$ajouter_argent <nom> <montant> [<motif>]` : **ajoute <montant>%s sur le compte de <nom>**. L'argent n'est pris nulle part. Si un motif est donné, il est communiqué à la personne.\n  `$enlever_argent <nom> <montant> [<motif>]` : **enlève <montant>%s sur le compte de <nom>**. L'argent est détruit. Si un motif est donné, il est communiqué à la personne.\n" % (currency, currency)

commandes_money = "  `$envoyer_argent <nom> <montant> <motif>` : **envoie <montant>%s de votre compte en banque à <nom>**. Le motif lui est communiqué.\n" % currency

commandes_msg = "  `$message_anonyme <destinataire> <message>` : **envoie un message anonyme** à un destinataire, qui peut être le nom d'une personne ou bien un nom de code.\n"

msg_help_extra = "\n__Note :__ Si un message, le nom d'une personne, d'une faction, etc. comporte des espaces, utilisez des **guillemets doubles** pour s'y référer dans les commandes, par exemple `$info_groupe \"La faction machin\"`"


#####
def get_contextual_help_added(faction, admin=True):
    contextual_help_added = "Vous êtes maintenant **%s de la faction %s** ! Félicitations ! %s\n" % ((faction.titre_admin if admin else faction.titre), faction.nom, ":crown:" if admin else ":spy:")
    contextual_help_added += "Vous pouvez accéder à ses salons privés et utiliser les commandes suivantes :\n"
    contextual_help_added += "`$info_groupe \"%s\"` : **liste les membres et les responables** de la faction\n" % faction.nom
    if(admin):
        contextual_help_added += "`$ajouter_membre nom_personne \"%s\"` : nomme la personne nom_personne comme membre de la faction %s - elle pourra connaître ses membres, ses responsables et accéder aux salons privés.\n" % (faction.nom, faction.nom)
        contextual_help_added += "`$enlever_membre nom_personne \"%s\"` : exclue le membre nom_personne de la faction %s (vous ne pouvez pas exclure un autre %s)\n" % (faction.nom, faction.nom, faction.titre_admin)
    contextual_help_added += "**Envoyez moi $help** si vous voulez que je vous rappelle ces infos."
    return contextual_help_added

def get_contextual_help_removed(faction, admin=True):
    if(admin):
        contextual_help_removed = "Vous n'êtes maintenant **plus %s de la faction %s** ! Vous êtes encore simple %s :spy:\n" % (faction.titre_admin, faction.nom, faction.titre)
        contextual_help_removed += "Vous ne pouvez plus ajouter ou exclure de membre, mais vous pouvez toujours accéder aux salons privés et connaître les membres et responsables."
    else:
        contextual_help_removed = "Vous n'êtes maintenant **plus %s de la faction %s** ! Vous ne pouvez plus accéder à ses salons privés.\n" % (faction.titre, faction.nom)
    contextual_help_removed += "\n**Envoyez moi $help** si vous voulez un résumé de ce que vous pouvez faire ou non..."
    return contextual_help_removed



@client.command(aliases=["statut", "help"])
@is_private()
async def hello(ctx):
    member = client.uid_as_member(ctx.author.id)
    msg = "**Bonjour, %s**. %s" % (get_name(member), hello_phrase)
    my_codenames = client.get_codenames(ctx.author.id)
    if(my_codenames):
        codenames_msg = "Vous êtes aussi joignable sous le(s) **nom(s) de code : {0}**.\n\n".format(", ".join(my_codenames))
    else:
        codenames_msg=""
    if(has_gm_privilege(ctx.author.id)):
        msg += " Vous êtes maître de jeu. :mage: (Compte bancaire : %d%s)\n\n" % (client.get_money(ctx.author.id), currency)
        msg += codenames_msg
        msg += "Les factions existantes sont : %s\n\n" % ", ".join(client.factions.keys())
        await ctx.send(msg)
        msg = "Vous pouvez utiliser les commandes suivantes :\n"
        msg += "__Commandes générales :__\n"
        msg += commandes_membre
        msg += commandes_msg
        await ctx.send(msg)
        msg = "__Commandes de gestionnaire de faction :__\n"
        msg += commandes_groupadmin
        await ctx.send(msg)
        msg = "__Commandes de maître de jeu :__\n"
        msg += commandes_gm
        await ctx.send(msg)
        msg = "__Commandes de gestion de l'argent :__\n"
        msg += commandes_gm_money
        msg += commandes_money
        msg += msg_help_extra
        await ctx.send(msg)
    else:
        msg += "\nSolde de votre compte en banque : **%d%s**" % (client.get_money(ctx.author.id), currency)
        msg += "\n\n"
        msg += codenames_msg
        msg += "Vous pouvez faire un virement bancaire avec :\n"
        msg += commandes_money
        msg += "Vous pouvez envoyer des messages anonymesavec :\n"
        msg += commandes_msg
        await ctx.send(msg)
        factions_membre = list()
        factions_admin = list()
        for f in client.factions.keys():
            if(has_group_admin_rights(ctx.author.id, f)):
                factions_admin.append(f)
            if(has_group_member_rights(ctx.author.id, f)):
                factions_membre.append(f)
        if((not factions_membre) and (not factions_admin)):
            msg = "\nJe ne peux rien faire d'autre pour vous pour l'instant, revenez quand vous vous serez fait des amis..."
        else:
            msg = ""
            if(factions_membre):
                msg += "\n__Vous êtes **membre** des factions :__ %s :spy:\n" % ", ".join(factions_membre)
                msg += "Vous pouvez utiliser avec elles les commandes suivantes :\n"
                msg += commandes_membre
            if(factions_admin):
                msg += "\n__Vous êtes **responsable** des factions :__ %s :crown:\n" % ", ".join(factions_admin)
                msg += "Vous pouvez utiliser avec elles les commandes suivantes :\n"
                msg += commandes_groupadmin
        msg += msg_help_extra
        await ctx.send(msg)


# Liste *toutes* les factions - GM seulement

@client.command()
@is_gm()
@is_private()
async def liste_groupes(ctx):
    await ctx.send("Liste des factions :")
    for f in client.factions.keys():
        txt = client.factions[f].display_fancy(client)
        await ctx.send(txt)
    await ctx.send("%d factions au total" % len(client.factions))

# Crée une faction - GM seulement

@client.command()
@is_gm()
@is_private()
async def ajouter_groupe(ctx, nom, channel_discord, titre_adm="chef", titre_membre="membre"):
    async with client.state_lock:
        if nom in client.factions:
            await ctx.send("La faction %s existe déjà !" % nom)
            return
        chans = client.my_server.channels
        # If we were given a section ID, get it !
        try:
            section_id = int(channel_discord)
        except ValueError:
            section_id = None
        #Find the section
        section = None
        for c in chans:
            if ((section_id and (section_id == c.id)) or (c.name.lower() == channel_discord.lower())): # Ignore case:
                section = c # Got it
                break
        if not section:
            await ctx.send("Le channel ou la catégorie '%s' n'existe pas ! Donnez un nom (ou un ID) de channel ou de catégorie SVP." % str(channel_discord))
            return
        # Check if it doesn't already exists:
        for (n,f) in client.factions.items():
            if(f.section_id == section.id):
                await ctx.send("Le channel ou la catégorie '%s' est déjà associé à la faction '%s' ! Vous avez du faire une erreur." % (str(channel_discord), n))
                return
        newfaction = Faction(nom, section.id, titre_membre=titre_membre, titre_admin=titre_adm)
        client.factions[nom] = newfaction
    await ctx.send("Faction %s crée !" % nom)
    everyone = client.my_server.default_role
    if ((everyone not in section.overwrites) or (section.overwrites_for(everyone).read_messages != False)):
        await ctx.send("WARNING: Le channel ou la catégorie '%s' était visible de tous, cela ne va pas du tout pour un groupe secret ! Elle sera maintenant seulement visible de ses membres." % nom)
        await section.set_permissions(everyone, read_messages=False)
    await client.log(ctx.author, ":heavy_plus_sign::classical_building: J'ai **crée la faction %s**" % nom)
    await savedata(client)

# Reconfigurer un groupe existant
@client.command()
@is_gm()
@is_private()
async def modifier_groupe(ctx, nom_faction, commande, nouvelle_valeur):
    async with client.state_lock:
        if nom_faction not in client.factions:
            await ctx.send("La faction %s n'existe pas !" % nom_faction)
            return
        if(commande == "salon"):
            chans = client.my_server.channels
            # If we were given a section ID, get it !
            try:
                section_id = int(nouvelle_valeur)
            except ValueError:
                section_id = None
            #Find the section
            section = None
            for c in chans:
                if ((section_id and (section_id == c.id)) or (c.name.lower() == nouvelle_valeur.lower())): # Ignore case:
                    section = c # Got it
                    break
            if not section:
                await ctx.send("Le channel ou la catégorie '%s' n'existe pas ! Donnez un nom (ou un ID) de channel ou de catégorie SVP." % str(channel_discord))
                return
            # Check if it doesn't already exists:
            for (n,f) in client.factions.items():
                if(f.section_id == section.id):
                    await ctx.send("Le channel ou la catégorie '%s' est déjà associé à la faction '%s' ! Vous avez du faire une erreur." % (str(channel_discord), n))
                    return
            await ctx.send("Salon %s trouvé, modification en cours..." % section.name)
            client.factions[nom_faction].section_id = section.id
            everyone = client.my_server.default_role
            if ((everyone not in section.overwrites) or (section.overwrites_for(everyone).read_messages != False)):
                await ctx.send("WARNING: Le channel ou la catégorie '%s' était visible de tous, cela ne va pas du tout pour un groupe secret ! Elle sera maintenant seulement visible de ses membres." % section.name)
                await section.set_permissions(everyone, read_messages=False)
            await ctx.send("Migration des membres...")
            for uid in client.factions[nom_faction].id_membres:
                await client.add_user_to_section(uid, section.id)
            await ctx.send("OK")
            await ctx.send("La faction %s est maintenant associée au salon '%s'. Aucune modification n'a été apportée à l'ancien salon (s'il existe). Vous pouvez le détruire si vous voulez." % (nom_faction, section.name))
            await client.log(ctx.author, ":arrows_clockwise::classical_building: J'ai **modifié la faction %s** qui est maintenant associée au **salon %s**" % (nom_faction, section.name))
        elif(commande == "titre"):
            client.factions[nom_faction].titre = nouvelle_valeur
            await ctx.send("Le titre des simples membres de la faction %s est maintenant : '%s'." % (nom_faction, nouvelle_valeur))
            await client.log(ctx.author, ":arrows_clockwise::classical_building: J'ai **modifié la faction %s** dont les **simples membres** ont maintenant pour **titre : '%s'**" % (nom_faction, nouvelle_valeur))
        elif(commande == "titre_admin"):
            client.factions[nom_faction].titre_admin = nouvelle_valeur
            await ctx.send("Le titre des chefs membres de la faction %s est maintenant : '%s'." % (nom_faction, nouvelle_valeur))
            await client.log(ctx.author, ":arrows_clockwise::classical_building: J'ai **modifié la faction %s** dont les **chefs** ont maintenant pour **titre : '%s'**" % (nom_faction, nouvelle_valeur))
    await savedata(client)



# Ajoute un admin à un groupe - GM seulement

@client.command()
@is_gm()
@is_private()
async def ajouter_admin(ctx, nom, groupe):
    async with client.state_lock:
        if groupe not in client.factions:
            await ctx.send("La faction %s n'existe pas !" % groupe)
            return
        result = client.find_by_name(nom)
        if result is None:
            await ctx.send("Impossible de trouver '%s' sur le serveur !" % nom)
            return
        uid, nick = result
        if client.factions[groupe].est_admin(uid):
            await ctx.send("%s est déjà %s de la faction %s!" % (nick, client.factions[groupe].titre_admin, groupe))
            return
        await client.factions[groupe].ajoute_admin(uid, client)
    await ctx.send("%s a été promu %s de la faction %s !" % (nick, client.factions[groupe].titre_admin, groupe))
    await client.uid_as_member(uid).send(get_contextual_help_added(client.factions[groupe], admin=True))
    await client.log(ctx.author, ":arrow_up::crown: J'ai **ajouté %s** comme **chef** de la faction **%s**" % (nick, groupe))
    await savedata(client)
    
    
# Retire un admin à un groupe - GM seulement

@client.command()
@is_gm()
@is_private()
async def enlever_admin(ctx, nom, groupe):
    async with client.state_lock:
        if groupe not in client.factions:
            await ctx.send("La faction %s n'existe pas !" % groupe)
            return
        result = client.find_by_name(nom)
        if result is None:
            await ctx.send("Impossible de trouver '%s' sur le serveur !" % nom)
            return
        uid, nick = result
        if not client.factions[groupe].est_admin(uid):
            await ctx.send("%s n'est pas %s de la faction %s!" % (nick, client.factions[groupe].titre_admin, groupe))
            return
        client.factions[groupe].retire_admin(uid)
    await ctx.send("%s n'est plus %s de la faction %s ! (mais est toujours simple membre)" % (nick, client.factions[groupe].titre_admin, groupe))
    await client.uid_as_member(uid).send(get_contextual_help_removed(client.factions[groupe], admin=True))
    await client.log(ctx.author, ":arrow_down::crown: J'ai **enlevé %s** comme **chef** de la faction **%s**" % (nick, groupe))
    await savedata(client)


# Ajoute un membre à un groupe - demande les droits admins sur le groupe

@client.command()
@is_private()
async def ajouter_membre(ctx, nom, groupe):
    async with client.state_lock:
        if((groupe not in client.factions) or (not has_group_admin_rights(ctx.author.id, groupe))):
            await ctx.send("Soit la faction %s n'existe pas, soit vous n'avez pas un rang suffisant dedans pour faire cela." % groupe)
            return
        result = client.find_by_name(nom)
        if result is None:
            await ctx.send("Impossible de trouver '%s' sur le serveur !" % nom)
            return
        uid, nick = result
        if client.factions[groupe].est_membre(uid):
            await ctx.send("%s est déjà %s de la faction %s !" % (nick, client.factions[groupe].titre, groupe))
            return
        await client.factions[groupe].ajoute_membre(uid, client)
    await ctx.send("%s a été initié %s de la faction %s !" % (nick, client.factions[groupe].titre, groupe))
    await client.uid_as_member(uid).send(get_contextual_help_added(client.factions[groupe], admin=False))
    await client.log(ctx.author, ":arrow_up::spy: J'ai **ajouté %s** comme **membre** de la faction **%s**" % (nick, groupe))
    await savedata(client)



# Retire un membre d'un groupe - demande les droits admins sur le groupe

@client.command()
@is_private()
async def enlever_membre(ctx, nom, groupe):
    async with client.state_lock:
        if((groupe not in client.factions) or (not has_group_admin_rights(ctx.author.id, groupe))):
            await ctx.send("Soit la faction %s n'existe pas, soit vous n'avez pas un rang suffisant dedans pour faire cela." % groupe)
            return
        result = client.find_by_name(nom)
        if result is None:
            await ctx.send("Impossible de trouver '%s' sur le serveur !" % nom)
            return
        uid, nick = result
        if not client.factions[groupe].est_membre(uid):
            await ctx.send("%s n'est pas %s de la faction %s !" % (nick, client.factions[groupe].titre, nom))
            return
        if(client.factions[groupe].est_admin(uid) and not has_gm_privilege(ctx.author.id)):
            await ctx.send("%s est également %s de la faction %s; vous ne pouvez pas l'exclure." % (nick, client.factions[groupe].titre_admin, nom))
            return
        await client.factions[groupe].retire_membre(uid, client, gm=has_gm_privilege(ctx.author.id))
    await ctx.send("%s a été exclu de la faction %s !" % (nick, groupe))
    await client.uid_as_member(uid).send(get_contextual_help_removed(client.factions[groupe], admin=False))
    await client.log(ctx.author, ":arrow_down::spy: J'ai **enlevé %s** comme **membre** de la faction **%s**" % (nick, groupe))
    await savedata(client)

# Affiche les infos d'un groupe - demande d'en être membre

@client.command()
@is_private()
async def info_groupe(ctx, groupe):
    if((groupe not in client.factions) or (not has_group_member_rights(ctx.author.id, groupe))):
        await ctx.send("Soit la faction %s n'existe pas, soit vous n'avez pas un rang suffisant dedans pour faire cela." % groupe)
        return
    txt = client.factions[groupe].display_fancy(client)
    await ctx.send(txt)


# Gestion des noms de code
@client.command(aliases=["ajouter_nom_de_code"])
@is_gm()
@is_private()
async def ajouter_alias(ctx, nom, codename):
    qui = client.find_by_name(nom)
    if qui is None:
        await ctx.send("Impossible de trouver '%s' sur le serveur !" % nom)
        return
    target_id, nompersonne = qui
    async with client.state_lock:
        if codename in client.codenames:
            await ctx.send("Ce nom de code est déjà utilisé, par {0}".format(get_name(client.uid_as_member(client.codenames[codename]))))
            return
    async with client.state_lock:
        client.codenames[codename] = target_id
    await ctx.send("Le nom de code '{0}' a été attribué à {1}".format(codename, nompersonne))
    await client.uid_as_member(target_id).send(":pen_fountain: Vous pouvez désormais être contacté sous le **nom de code '{0}'**.".format(codename))
    await client.log(ctx.author, (":pen_fountain: J'ai **ajouté le nom de code {0}** à **{1}**".format(codename, nompersonne)))
    await savedata(client)


@client.command(aliases=["enlever_nom_de_code"])
@is_gm()
@is_private()
async def enlever_alias(ctx, codename):
    async with client.state_lock:
        if codename not in client.codenames:
            await ctx.send("Ce nom de code n'existe pas.")
            return
    async with client.state_lock:
        old_target_id = client.codenames[codename]
        del client.codenames[codename]
    old_target_name = get_name(client.uid_as_member(old_target_id))
    await ctx.send("**Le nom de code \"{0}\"**, précédemment attribué à {1}, **a été supprimé**.".format(codename, old_target_name))
    await client.uid_as_member(old_target_id).send(":pen_fountain::no_entry_sign: **Le nom de code \"{0}\" vous a été retiré**, vous ne pouvez plus être contacté sous ce nom.".format(codename))
    await client.log(ctx.author, (":pen_fountain::no_entry_sign: J'ai **supprimé le nom de code {0}**, précédemment attribué à {1}.".format(codename, old_target_name)))
    await savedata(client)


# Gestion des noms de code
@client.command(aliases=["alias", "noms_de_code"])
@is_gm()
@is_private()
async def aliases(ctx):
    async with client.state_lock:
        copy_aliases = dict(client.codenames)
    noms = sorted(copy_aliases.keys())
    await ctx.send("{0} noms de code/alias déclarés :".format(len(noms)))
    for pagestart in range(0, len(noms), 10): # 10 names at a time
        data = "\n".join(["- {0} : {1}".format(k, get_name(client.uid_as_member(copy_aliases[k]))) for k in noms[pagestart:pagestart+10]])
        await ctx.send(data)

# Envoi de message
@client.command()
@is_private()
async def message_anonyme(ctx, destinataire, message):
    # Trouver le destinataire - Chercher d'abord dans les noms de code
    async with client.state_lock:
        dst = client.find_codename(destinataire, case_insensitive=True)
    if(dst): # c'est un nom de code !
        target_id, target_realname = dst
        await client.uid_as_member(target_id).send(":mailbox_with_mail: **Vous avez reçu le message suivant**, adressé à \"{0}\" : \"{1}\"".format(destinataire, message))
        await ctx.send(":incoming_envelope: Votre message a bien été transmis à \"{0}\"".format(destinataire))
        await client.log(ctx.author, (":incoming_envelope: J'ai **envoyé à \"{0}\"** (c'est à dire {1}) **le message suivant** : \"{2}\"".format(destinataire, target_realname, message)))
    else:
        dst = client.find_by_name(destinataire, case_insensitive=True)
        if(dst): # c'est un nom "normal" !
            target_id, target_realname = dst
            await client.uid_as_member(target_id).send(":mailbox_with_mail: **Vous avez reçu le message suivant**, adressé à votre nom ({0}) : \"{1}\"".format(destinataire, message))
            await ctx.send(":incoming_envelope: Votre message a bien été transmis à {0}".format(destinataire))
            await client.log(ctx.author, (":incoming_envelope: J'ai **envoyé à \"{0}\" le message suivant** : \"{1}\"".format(destinataire, message)))
        else:
            await ctx.send(":envelope::no_entry_sign: Impossible de trouver \"{0}\", cela doit être le nom d'une personne ou un nom de code".format(destinataire))
            await client.log(ctx.author, (":envelope::no_entry_sign: **J'ai tenté d'envoyer à \"{0}\"** le message suivant : \"{1}\". **Cela n'a pas fonctionné** car le bot n'a pas pu trouver \"{0}\"".format(destinataire, message)))



# Gestion de l'argent
@client.command()
@is_gm()
@is_private()
async def ajouter_argent(ctx, nom, montant, motif=None):
    qui = client.find_by_name(nom)
    if qui is None:
        await ctx.send("Impossible de trouver '%s' sur le serveur !" % nom)
        return
    target_id, nom = qui
    amount = -1
    try:
        amount = int(montant)
    except ValueError:
        pass
    if(amount <= 0):
        await ctx.send("Veuillez entrer un montant entier strictement positif (pour enlever de l'argent, utilisez $enlever_argent).")
        return
    async with client.state_lock:
        argent_courant = client.get_money(target_id)
        client.set_money(target_id, argent_courant+amount)
    await ctx.send("%d%s crédités à %s (nouveau solde: %d%s)" % (amount, currency, nom, (argent_courant+amount), currency))
    await client.uid_as_member(target_id).send((":moneybag: Votre compte bancaire a été **crédité de %d%s**. Nouveau solde: **%d%s**." %(amount, currency, (argent_courant+amount), currency)) +((" (Motif : %s)" % motif) if motif else ""))
    await client.log(ctx.author, (":arrow_up::moneybag: J'ai **ajouté %d%s** au compte bancaire de **%s**" % (amount, currency, nom)) + ((" (Motif : %s)" % motif) if motif else ""))
    await savedata(client)


@client.command()
@is_gm()
@is_private()
async def enlever_argent(ctx, nom, montant, motif=None):
    qui = client.find_by_name(nom)
    if qui is None:
        await ctx.send("Impossible de trouver '%s' sur le serveur !" % nom)
        return
    target_id, nom = qui
    amount = -1
    try:
        amount = int(montant)
    except ValueError:
        pass
    if(amount <= 0):
        await ctx.send("Veuillez entrer un montant entier strictement positif.")
        return
    async with client.state_lock:
        argent_courant = client.get_money(target_id)
        if(amount > argent_courant):
            await ctx.send("%s n'a que %d%s, vous ne pouvez pas lui enlever  %d%s" % (nom, argent_courant, currency, amount, currency))
            return
        client.set_money(target_id, argent_courant-amount)
    await ctx.send("%d%s débités à %s (nouveau solde: %d%s)" % (amount, currency, nom, (argent_courant-amount), currency))
    await client.uid_as_member(target_id).send((":money_with_wings: Votre compte bancaire a été **débité de %d%s**. Nouveau solde: **%d%s**."%(amount, currency, (argent_courant-amount), currency))+((" (Motif : %s)" % motif) if motif else ""))
    await client.log(ctx.author, (":arrow_down::moneybag: J'ai **enlevé %d%s** au compte bancaire de **%s**" % (amount, currency, nom)) + ((" (Motif : %s)" % motif) if motif else ""))
    await savedata(client)

@client.command()
@is_gm()
@is_private()
async def banque(ctx, ordre="alpha"):
    async with client.state_lock:
        accountdata = list(client.bank_accounts.items())
    comptes = []
    totalmoney = 0
    for (k,v) in accountdata:
        member = client.uid_as_member(k)
        if(member):
            comptes.append((get_name(member),v))
            totalmoney += v
    if(ordre == "-"):
        sortfunc = lambda e:-e[1] # Ordre décroissant d'argent
    elif(ordre == "+"):
        sortfunc = lambda e:e[1] # Ordre croissant d'argent
    else:
        sortfunc = lambda e:e[0].lower() # Ordre lexicographique du nom
    comptes.sort(key=sortfunc)
    await ctx.send("**%d comptes bancaires connus :**" % len(comptes))
    for pagestart in range(0, len(comptes), 10): # 10 accounts at a time
        data = "\n".join(["- %s : %d%s" %(n,v, currency) for (n,v) in comptes[pagestart:pagestart+10]])
        await ctx.send(data)
    await ctx.send("__Masse monétaire totale en circulation :__ %d%s" % (totalmoney, currency))
    await ctx.send("Si certaines personnes n'apparaissent pas dans cette liste, elles ont un compte vide. Ce n'est pas un problème ni un bug.")

# Gestion de l'argent
@client.command()
@is_private()
async def envoyer_argent(ctx, nom, montant, motif):
    async with client.state_lock:
        qui = client.find_codename(nom, case_insensitive=True)
    if qui is None:
        qui = client.find_by_name(nom)
        target_anon = False
    else:
        target_anon = True
    
    if qui is None:
        await ctx.send("Impossible de trouver '%s', cela doit être un nom ou un nom de code !" % nom)
        return
    target_id, vrainom = qui
    if (target_id ==ctx.author.id):
        await ctx.send("Vous ne pouvez pas envoyer de l'argent à vous-même !")
        return
    amount = -1
    try:
        amount = int(montant)
    except ValueError:
        pass
    if(amount <= 0):
        await ctx.send("Veuillez entrer un montant entier strictement positif.")
        return
    async with client.state_lock:
        my_money = client.get_money(ctx.author.id)
        if(my_money < amount):
            await ctx.send("Vous n'avez que %d%s, vous ne pouvez pas donner %d%s" % (my_money, currency, amount, currency))
            return
        their_money = client.get_money(target_id)
        client.set_money(ctx.author.id, my_money-amount)
        client.set_money(target_id, their_money+amount)
    if(target_anon):
        await ctx.send(":money_with_wings: Vous avez **envoyé anonymement %d%s à \"%s\"**. Nouveau solde :** %d%s**" % (amount, currency, nom, (my_money-amount), currency))
        await client.uid_as_member(target_id).send(":moneybag: Vous **avez reçu %d%s en tant que \"%s\"**. Nouveau solde: **%d%s**. Motif : %s." % (amount, currency, nom, (their_money+amount), currency, motif))
        await client.log(ctx.author, (":money_with_wings: J'ai **envoyé anonymement %d%s** à \"**%s**\" (c'est à dire %s). Motif : %s" % (amount, currency, nom, vrainom, motif)))
    else:
        await ctx.send(":money_with_wings: Vous avez **envoyé %d%s à %s**. Nouveau solde :** %d%s**" % (amount, currency, vrainom, (my_money-amount), currency))
        await client.uid_as_member(target_id).send(":moneybag: Vous **avez reçu %d%s de %s**. Nouveau solde: **%d%s**. Motif : %s." % (amount, currency, get_name(ctx.author), (their_money+amount), currency, motif))
        await client.log(ctx.author, (":money_with_wings: J'ai **envoyé %d%s** à **%s**. Motif : %s" % (amount, currency, vrainom, motif)))
    await savedata(client)


# Load/save
@client.command()
@is_gm()
@is_private()
async def save(ctx):
    await savedata(client)
    await ctx.send("Sauvegarde effectuée dans %s (%d factions)" % ("%d.json" % server_managed, len(client.factions)))
    if(ctx is not client.log_channel):
        await client.log(ctx.author, ":arrow_right::floppy_disk: J'ai **sauvegardé les informations du bot** sur le disque")


@client.command()
@is_gm()
@is_private()
async def load(ctx, forcing=False):
    force = (forcing == "force")
    with open(server_save_file,'r') as jsfile:
        data = json.load(jsfile)
    await ctx.send("Fichier %s lu" % ("%d.json" % server_managed))
    if("factiondata" not in data.keys()):
        await ctx.send("Attention: ancien format de fichier de sauvegarde, pas de comptes bancaires lus")
        factiondata = data
        accounts = None
    else: # Version >= 1
        factiondata = data["factiondata"]
        accounts = data["accounts"]
        if(data["version"] >= 2):
            codenames = data["codenames"]
        else:
            codenames = None
    async with client.state_lock:
        client.factions.clear()
        cat_ids = list([c.id for c in client.my_server.channels])
        for (k,f) in factiondata.items():
            await ctx.send("Chargement de la faction %s..." % k)
            cid = f["section_id"]
            if cid not in cat_ids:
                if not force:
                    await ctx.send("ERREUR : impossible de trouver le channel ou la catégorie d'ID %d (correspondant aux salons de la faction %s). La faction ne sera pas chargée." % (cid,k))
                    continue
                else:
                    await ctx.send("WARNING : impossible de trouver le channel ou la catégorie d'ID %d (correspondant aux salons de la faction %s). La faction sera chargée quand même (force) mais la mise à jour des salons posera une erreur." % (cid,k))
            faction = Faction(nom=f["nom"], section_id=cid, titre_membre=f["titre"], titre_admin=f["titre_admin"])
            faction.id_membres = list(f["id_membres"])
            faction.id_admins = list(f["id_admins"])
            client.factions[k] = faction
    await ctx.send("%d factions chargées : %s\n\n" % (len(client.factions), ", ".join(client.factions.keys())))
    if(accounts):
        async with client.state_lock:
            client.bank_accounts = dict({int(k):v for (k,v) in accounts.items()})
        await ctx.send("Comptes bancaires rechargés")
    if(codenames):
        async with client.state_lock:
            client.codenames = dict(codenames)
        await ctx.send("Noms de code rechargés")
    if(ctx is not client.log_channel):
        await client.log(ctx.author, ":arrow_left::floppy_disk: J'ai **rechargé les informations du bot** depuis le disque")
    await update(ctx)


            
@client.command()
@is_gm()
@is_private()
async def update(ctx):
    async with client.state_lock:
        await ctx.send("Vérification des informations pour %d factions..." % len(client.factions))
        cat_ids = list([c.id for c in client.my_server.channels])
        did_update = False
        for(k,f) in client.factions.items():
            if f.section_id not in cat_ids:
                 await ctx.send("ERREUR : impossible de trouver le channel ou la catégorie d'ID %d (correspondant aux salons de la faction %s). Aucune vérification ne sera faite pour cette faction. Modifiez son salon si celui qui lui était attribué a été détruit." % (f.section_id,k))
                 continue
            actual_members_id = set([c.id for c in client.get_users_in_section(f.section_id)])
            known_members_id = set(f.id_membres)
            membres_ok = actual_members_id.intersection(known_members_id)
            missing_members = actual_members_id - known_members_id
            extra_members = known_members_id - actual_members_id
            if (not missing_members) and (not extra_members):
                await ctx.send("- Faction **%s** : OK (aucune mise à jour nécessaire)." % k)
                continue
            did_update = True
            msg = "- Faction **%s** :" % k
            if(membres_ok):
                msg += "\n  * RAS pour %d membres" % len(membres_ok)
            for m in missing_members:
                msg += "\n  * %s semble être membre, mais je ne l'avais pas noté comme membre. Je l'ajoute." % get_name(client.uid_as_member(m))
                client.factions[k].id_membres.append(m)
            for m in extra_members:
                msg += "\n  * %s semble ne semble plus être membre, alors que je l'avais noté comme membre. Je l'enlève." % get_name(client.uid_as_member(m))
                client.factions[k].id_membres.remove(m)
                if m in client.factions[k].id_admins:
                    client.factions[k].id_admins.remove(m)
                    msg += " (Je l'enlève aussi comme admin.)"
            await ctx.send(msg)
    if(ctx is not client.log_channel):
        await client.log(ctx.author, ":arrows_clockwise: J'ai **mis à jour les informations du bot** avec l'état de Discord")
    if(did_update):
        await ctx.send("Des données ont été mises à jour. Sauvegarde en cours...")
        await save(ctx)
    else:
        await ctx.send("Tout est OK, vous pouvez reprendre une activité normale.")

# Gestion d'erreurs


@liste_groupes.error
@ajouter_groupe.error
@enlever_admin.error
@ajouter_admin.error
@ajouter_argent.error
@enlever_argent.error
@banque.error
@modifier_groupe.error
@ajouter_alias.error
@enlever_alias.error
@aliases.error
async def GM_only_error(ctx, error):
    if isinstance(error, commands.CheckFailure):
        await ctx.send("Je ne peux parler de cela que dans une discussion privée, et avec le maître de jeu.")
    elif isinstance(error, commands.errors.MissingRequiredArgument):
        await ctx.send("Il manque le paramètre : %s. **Tapez $help pour savoir comment utiliser les commandes**" % error.param.name)
    else:
        print("{} : Erreur inconnue (GM_only_error) : '{}'".format(datetime.now().strftime("%d/%m %H:%M:%S"), error))
        await ctx.send("Erreur inconnue (GM_only_error) : '%s'. Merci de signaler au développaire." % error)


@ajouter_membre.error
@enlever_membre.error
@info_groupe.error
@envoyer_argent.error
@message_anonyme.error
async def other_error(ctx, error):
    if isinstance(error, commands.CheckFailure):
        await ctx.send("Je ne peux parler de cela que dans une discussion privée.")
    elif isinstance(error, commands.errors.MissingRequiredArgument):
        await ctx.send("Il manque le paramètre : %s. **Tapez $help pour savoir comment utiliser les commandes**" % error.param.name)
    else:
        print("{} : Erreur inconnue (other_error) : '{}'".format(datetime.now().strftime("%d/%m %H:%M:%S"), error))
        await ctx.send("Erreur inconnue (other_error) : '%s'. Merci de signaler au développaire." % error)


@hello.error
async def hello_error(ctx, error):
    if isinstance(error, commands.CheckFailure):
        await ctx.send("Bonjour. Je préfère discuter en privé. **Envoyez moi $help dans un message privé.**")
    elif isinstance(error, commands.errors.MissingRequiredArgument):
        await ctx.send("Il manque le paramètre : %s. **Tapez $help pour savoir comment utiliser les commandes**" % error.param.name)
    else:
        print("{} : Erreur inconnue (hello_error) : '{}'".format(datetime.now().strftime("%d/%m %H:%M:%S"), error))
        await ctx.send("Erreur inconnue (hello_error) : '%s'. Merci de signaler au développaire." % error)


@client.event
async def on_command_error(ctx, error):
    if isinstance(error, CommandNotFound):
        await ctx.send("Commande inconnue. **Tapez $help pour connaître les commandes disponibles.**")
        return
    if isinstance(error, commands.CheckFailure):
        pass # Weird. But not a problem.
    else:
        await ctx.send("Erreur inconnue (on_command_error) : '%s'. Merci de signaler au développaire." % error)
        print("{} : Erreur inconnue (on_command_error) : '{}'".format(datetime.now().strftime("%d/%m %H:%M:%S"), error))
        return




client.run(token)
